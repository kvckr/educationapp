﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EducationApp.Startup))]
namespace EducationApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Web;
using EducationApp.Models;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext db = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
        private GenericRepository<Entry> _entryRepository;
        private GenericRepository<UserRate> _userRatesRepository;
        private GenericRepository<Tag> _tagsRepository;
        private GenericRepository<Template> _templatesRepository;
        private GenericRepository<Medal> _medalsRepository;
        private GenericRepository<Comment> _commentsRepository;





        public GenericRepository<Entry> Entries
        {
            get
            {
                if (_entryRepository == null)
                    _entryRepository = new GenericRepository<Entry>(db);
                return _entryRepository;
            }
        }

        public GenericRepository<Comment> Comments
        {
            get
            {
                if (_commentsRepository == null)
                    _commentsRepository = new GenericRepository<Comment>(db);
                return _commentsRepository;
            }
        }

        public GenericRepository<UserRate> UserRates
        {
            get
            {
                if (_userRatesRepository == null)
                    _userRatesRepository = new GenericRepository<UserRate>(db);
                return _userRatesRepository;
            }
        }

        public GenericRepository<Tag> Tags
        {
            get
            {
                if (_tagsRepository == null)
                    _tagsRepository = new GenericRepository<Tag>(db);
                return _tagsRepository;
            }
        }

        public GenericRepository<Template> Templates
        {
            get
            {
                if (_templatesRepository == null)
                    _templatesRepository = new GenericRepository<Template>(db);
                return _templatesRepository;
            }
        }

        public GenericRepository<Medal> Medals
        {
            get
            {
                if (_medalsRepository == null)
                    _medalsRepository = new GenericRepository<Medal>(db);
                return _medalsRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿using EducationApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;


namespace EducationApp.Filters
{
    public class UnbanAttribute : FilterAttribute, IActionFilter
    {
        private ApplicationUserManager userManager;

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            ApplicationUser user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            if (user != null)
                if (user.LockoutEndDateUtc != null)
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}
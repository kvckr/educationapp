namespace EducationApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class q : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Likes", "Comment_Id", "dbo.Comments");
            DropForeignKey("dbo.UserRates", "Entry_Id", "dbo.Entries");
            DropIndex("dbo.UserRates", new[] { "Entry_Id" });
            DropIndex("dbo.Likes", new[] { "Comment_Id" });
            RenameColumn(table: "dbo.Likes", name: "Comment_Id", newName: "CommentId");
            RenameColumn(table: "dbo.UserRates", name: "Entry_Id", newName: "EntryId");
            AlterColumn("dbo.UserRates", "EntryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Likes", "CommentId", c => c.Int(nullable: false));
            CreateIndex("dbo.UserRates", "EntryId");
            CreateIndex("dbo.Likes", "CommentId");
            AddForeignKey("dbo.Likes", "CommentId", "dbo.Comments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserRates", "EntryId", "dbo.Entries", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRates", "EntryId", "dbo.Entries");
            DropForeignKey("dbo.Likes", "CommentId", "dbo.Comments");
            DropIndex("dbo.Likes", new[] { "CommentId" });
            DropIndex("dbo.UserRates", new[] { "EntryId" });
            AlterColumn("dbo.Likes", "CommentId", c => c.Int());
            AlterColumn("dbo.UserRates", "EntryId", c => c.Int());
            RenameColumn(table: "dbo.UserRates", name: "EntryId", newName: "Entry_Id");
            RenameColumn(table: "dbo.Likes", name: "CommentId", newName: "Comment_Id");
            CreateIndex("dbo.Likes", "Comment_Id");
            CreateIndex("dbo.UserRates", "Entry_Id");
            AddForeignKey("dbo.UserRates", "Entry_Id", "dbo.Entries", "Id");
            AddForeignKey("dbo.Likes", "Comment_Id", "dbo.Comments", "Id");
        }
    }
}

﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace EducationApp.Cloud
{

    public class ImageUploader
    {
        private CloudinaryDotNet.Cloudinary _cloudinary;

        public ImageUploader()
        {
            var account = new Account(
                "iedapp",
                "517463638481355",
                "I1kc6ajVFyPBe29PMASKbIVgEc8");

            _cloudinary = new CloudinaryDotNet.Cloudinary(account);
        }

        public string UploadBase64Image(string base64Image)
        {
            var uploadParams = new ImageUploadParams
            {
                File = new FileDescription(base64Image)
            };
            var uploadResult = _cloudinary.Upload(uploadParams);
            return uploadResult.PublicId;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public virtual ApplicationUser User { get; set; }
        public int EntryId { get; set; }
        public Entry Entry { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
    }
}
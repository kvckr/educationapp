﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EducationApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.

    public enum Gender
    {
        Male = 0,
        Female
    }

    public enum Theme
    {
        Light = 0,
        Dark
    }

    public enum Language
    {
        ru = 0,
        en
    }

    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Entries = new List<Entry>();
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public Theme Theme { get; set; }
        public Language? Language { get; set; }
        public virtual ICollection<Entry> Entries { get; set; }
        public virtual ICollection<Medal> Medals { get; set; } 
        public virtual ICollection<Comment> Comments { get; set; } 

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    class MyContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext db)
        {
            Template t1 = new Template() { Name = "Default",TemplateEdit = "~/Views/Shared/Template1.cshtml", TemplateRender = "~/Views/Shared/Template1Render.cshtml" };
            Template t2 = new Template() { Name = "With map", TemplateEdit = "~/Views/Shared/Template2.cshtml", TemplateRender = "~/Views/Shared/Template2Render.cshtml" };
            Template t3 = new Template() { Name = "Inline image", TemplateEdit = "~/Views/Shared/Template3.cshtml", TemplateRender = "~/Views/Shared/Template3Render.cshtml" };
            Template t4 = new Template() { Name = "With youtube", TemplateEdit = "~/Views/Shared/Template4.cshtml", TemplateRender = "~/Views/Shared/Template4Render.cshtml" };
            Medal m1 = new Medal() { Description = "Fill profile", ImageId = "medal_profile" };
            Medal m2 = new Medal() { Description = "Bad guy", ImageId = "medal_ban" };
            Medal m3 = new Medal() { Description = "30 rating", ImageId = "medal_rating" };
            Medal m4 = new Medal() { Description = "10 comments", ImageId = "medal_comments" };
            Medal m5 = new Medal() { Description = "10 entries", ImageId = "medal_entries" };



            db.Templates.Add(t1);
            db.Templates.Add(t2);
            db.Templates.Add(t3);
            db.Templates.Add(t4);
            db.Medals.Add(m1);
            db.Medals.Add(m2);
            db.Medals.Add(m3);
            db.Medals.Add(m4);
            db.Medals.Add(m5);

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));

            var role1 = new IdentityRole { Name = "admin" };

            roleManager.Create(role1);
                            
            var admin = new ApplicationUser { Email = "admin@admin.com", UserName = "admin",
                LockoutEnabled = true, ImageUrl = "https://res.cloudinary.com/iedapp/image/upload/default"
            };
            string password = "qweqweQ1!";
            var result = userManager.Create(admin, password);

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, role1.Name);
            }

            

            db.SaveChanges();
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
            Database.SetInitializer(new MyContextInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Comment>()
            .HasRequired(t => t.Entry)
            .WithMany(t => t.Comments)
            .HasForeignKey(d => d.EntryId)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Like>()
.HasRequired(t => t.Comment)
.WithMany(t => t.Likes)
.HasForeignKey(d => d.CommentId)
.WillCascadeOnDelete(true);
            modelBuilder.Entity<UserRate>()
.HasRequired(t => t.Entry)
.WithMany(t => t.UserRates)
.HasForeignKey(d => d.EntryId)
.WillCascadeOnDelete(true);
        }



        public DbSet<Entry> Entries { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Template> Templates { get; set; }

        public DbSet<UserRate> UserRates { get; set; }

        public DbSet<Medal> Medals { get; set; } 


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
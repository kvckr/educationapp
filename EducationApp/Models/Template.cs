﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class Template
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TemplateEdit { get; set; }
        public string TemplateRender { get; set; }
    }
}
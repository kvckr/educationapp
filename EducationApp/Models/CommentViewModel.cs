﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class CommentViewModel
    {
        public string Text { get; set; }
        public string Time { get; set; }
        public string UserImage { get; set; }
        public string UserName { get; set; }
        public int LikesCount { get; set; }
        public int Id { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace EducationApp.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string TagName { get; set; }

        public virtual ICollection<Entry> Entries { get; set; } = new HashSet<Entry>();
    }
}
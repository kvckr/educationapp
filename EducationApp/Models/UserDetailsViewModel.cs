﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EducationApp.Models
{
    public class UserDetailsViewModel
    {
        [Display(Name = "Name", ResourceType = typeof(Resources.Resource))]
        public string Name { get; set; }

        [Display(Name = "Surname", ResourceType = typeof(Resources.Resource))]
        public string Surname { get; set; }

        [Display(Name = "DateOfBirth", ResourceType = typeof(Resources.Resource))]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/2012")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(Resources.Resource))]
        public Gender? Gender { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resources.Resource))]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Theme", ResourceType = typeof(Resources.Resource))]
        public Theme Theme { get; set; }

        [Display(Name = "Language", ResourceType = typeof(Resources.Resource))]
        public Language? Language { get; set; }

        public string UserId { get; set; }
    }
}
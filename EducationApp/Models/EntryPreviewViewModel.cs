﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class EntryPreviewViewModel
    {
        public string CreateTime { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageId { get; set; }
        public Categories Category { get; set; }
        public int Id { get; set; }
        public int Rating { get; set; }
        public int CommentsCount { get; set; }
        public List<string> Tags { get; set; } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class Medal
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImageId { get;set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace EducationApp.Models
{
    public class EntryViewModel
    {
        public Template Template { get; set; }
        public List<Tag> Tags { get; set; }
        public DateTime CreateTime { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string ImageId { get; set; }
        public int Rating { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string YoutubeLink { get; set; }
        public Categories Category { get; set; }
        public int Id { get; set; }
    }
}
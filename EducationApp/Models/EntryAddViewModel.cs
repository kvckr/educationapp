﻿using EducationApp.Controllers;

namespace EducationApp.Models
{
    public class EntryAddViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string YoutubeLink { get; set; }
        public Categories Category { get; set; }
        public int TemplateId { get; set; }
        public HomeController.TagText[] Tags { get; set; }
    }
}
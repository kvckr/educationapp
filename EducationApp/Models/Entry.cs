﻿using System;
using System.Collections.Generic;

namespace EducationApp.Models
{
    public enum Categories
    {
        Tech = 0,
        Medicine,
        Programming,
        History,
        Biology,
        Chemistry,
    }

    public class Entry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string ImageId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string YoutubeLink { get; set; }
        public int Rating { get; set; }
        public DateTime CreateTime { get; set; }
        public Categories Category { get; set; }

        public virtual ICollection<Tag> Tags { get; set; } = new HashSet<Tag>();

        public virtual Template Template { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<UserRate> UserRates { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class UserRate
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public bool IsPositive { get; set; }
        public Entry Entry { get; set; }
        public int EntryId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
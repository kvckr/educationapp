﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EducationApp.Models
{
    public class Like
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public Comment Comment { get; set; }
        public int CommentId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
﻿# CoffeeScript

'use strict';

angular.module('myApp', [ 'ngTagsInput', 'hc.marked','uiGmapgoogle-maps','infinite-scroll'])

angular.module('myApp').filter "encodeUri", ->
  (x) ->
    encodeURIComponent x

angular.module('myApp')
  .directive('fileDropzone', () ->
    restrict: 'A'
    scope: {
      file: '='
      fileName: '='
      imageup: '&'
    }
    link: (scope, element, attrs) ->

      # function to prevent default behavior (browser loading image)
      processDragOverOrEnter = (event) ->
        event?.preventDefault()
        event.dataTransfer.effectAllowed = 'copy'
        false

      validMimeTypes = attrs.fileDropzone
      checkSize = (size) ->
        if attrs.maxFileSize in [undefined, ''] or (size / 1024) / 1024 < attrs.maxFileSize
          true
        else
          alert "File must be smaller than #{attrs.maxFileSize} MB"
          false

      isTypeValid = (type) ->
        if validMimeTypes in [undefined, ''] or validMimeTypes.indexOf(type) > -1
          true
        else
          alert "Invalid file type.  File must be one of following types #{validMimeTypes}"
          false


      element.bind 'dragover', processDragOverOrEnter
      element.bind 'dragenter', processDragOverOrEnter


      element.bind 'drop', (event) ->
        event?.preventDefault()
        reader = new FileReader()
        reader.onload = (evt) ->

          if checkSize(size) and isTypeValid(type)
            scope.$apply ->
              scope.file = evt.target.result
              scope.imageup()
              scope.fileName = name if angular.isString scope.fileName

        file = event.dataTransfer.files[0]
        name = file.name
        type = file.type
        size = file.size
        reader.readAsDataURL(file)
        return false
  )

angular.module("myApp").controller "avatarController", [ "$scope", "$http", ($scope, $http) ->
  getImage = undefined
  $scope.image = null
  $scope.isNewImage = false
  $scope.isLoading = false
  $scope.imageUpdate = ->
    $scope.isNewImage = true

  $scope.sendImage = (id) ->
    $scope.isLoading = true
    q =
      data: $scope.image
      id: id
    $http(
      method: "POST"
      url: "/Manage/UpdateImage/"
      data: q
    ).then (successCallback = (response) ->
      $scope.isLoading = false
      $scope.isNewImage = false
    )

  $scope.getCurrentUserImage = (id) ->
    q = id: id
    $http(
      method: "POST"
      url: "/Manage/GetCurrentUserImage/"
      data: q
    ).then (successCallback = (response) ->
      $scope.isImageSet = false
      $scope.isNewImage = false
      $scope.image = response.data
    )

 ]

angular.module('myApp').controller "tagCloudCtrl", [ "$scope", "$http", ($scope, $http) ->
  
  # This will make the "words" array visible from associated template
  $scope.words = []
  $scope.loadtags = ->
    $http(
      method: "GET"
      url: "/Home/GetTagsForCloud"
    ).then (successCallback = (response) ->
      sizes = []
      response.data.forEach (item) ->
        sizes.push item.Uses

      max = Math.max(sizes...)
      min = Math.min(sizes...)
      fontMin = 1
      fontMax = 6
      for i of response.data
        tag = response.data[i]
        size = (if tag.Uses is min then fontMin else (tag.Uses / max) * (fontMax - fontMin) + fontMin)
        lol =
          TagName: tag.TagName
          Weight: Math.round(size)

        $scope.words.push lol
    )
  $scope.loadtags()
]

angular.module("myApp").config (uiGmapGoogleMapApiProvider) ->
  uiGmapGoogleMapApiProvider.configure
    v: "3"
    libraries: "weather,geometry,visualization"



angular.module("myApp").config([ "markedProvider", (markedProvider) ->
  markedProvider.setOptions gfm: true
 ]).controller "createPostController", [ "$scope","$http", "uiGmapGoogleMapApi","$window", ($scope, $http , uiGmapGoogleMapApi,$window) ->
  uiGmapGoogleMapApi.then (maps) ->
    angular.extend $scope,
      map:
        center:
          latitude: 42.3349940452867
          longitude: -71.0353168884369
    
        zoom: 11
        marker : 
          id : 0
          coords:
            latitude: null
            longitude: null
        events:
          click: (map, eventName, originalEventArgs) ->
            e = originalEventArgs[0]
            lat = e.latLng.lat()
            lon = e.latLng.lng()
            $scope.map.marker =
              id: Date.now()
              coords:
                latitude: lat
                longitude: lon
    
            $scope.$apply()
  
  $http.get("../Template/GetTemplates").success (data) ->
    $scope.templates = data
    $scope.template = $scope.templates[0]
  $http.get("../Home/GetCategories").success (data) ->
    $scope.categories = data
    $scope.category = $scope.categories[0]
  window.onbeforeunload = (event) ->
    if $scope.isUploading
      message = "Sure you want to leave?"
      event = window.event  if typeof event is "undefined"
      event.returnValue = message  if event
      message
  $scope.my_markdown = ""
  $scope.tags = []
  $scope.isUploading = false
  $scope.loadTags = (query) ->
    $http.get "/Home/GetTagsForAutocomplete/" + query
  $scope.addPost = ->   
    $scope.isUploading = true
    q =
      Category: $scope.category.id
      TemplateId: $scope.template.Id
      Title: $scope.title
      Description: $scope.description
      Content: $scope.my_markdown
      Tags: $scope.tags
      Image: $scope.image
      Latitude: $scope.map.marker.coords.latitude
      Longitude: $scope.map.marker.coords.longitude
      YoutubeLink: $scope.link
    $http(
      method: "POST"
      url: "/Home/AddEntry"
      data: q
    ).then (successCallback = (response) ->
        $scope.isUploading = false
        $window.location.href = '/View/Index/'+response.data.toString();
      )
 ]

angular.module("myApp").controller "editPostController", [ "$scope","$http", "$location", "uiGmapGoogleMapApi","$window", ($scope, $http,$location , uiGmapGoogleMapApi,$window) ->
  uiGmapGoogleMapApi.then (maps) ->
    angular.extend $scope,
      map:
        center:
          latitude: 42.3349940452867
          longitude: -71.0353168884369
    
        zoom: 11
        marker : 
          id : 0
          coords:
            latitude: null
            longitude: null
        events:
          click: (map, eventName, originalEventArgs) ->
            e = originalEventArgs[0]
            lat = e.latLng.lat()
            lon = e.latLng.lng()
            $scope.map.marker =
              id: Date.now()
              coords:
                latitude: lat
                longitude: lon
            $scope.$apply()
     $scope.getPost()
  $scope.id = $location.absUrl().split(/[/ ]+/).pop()
  $scope.fillData = (entry) ->
    $scope.my_markdown = entry.Content
    $scope.title = entry.Title
    $scope.description = entry.Description
    $scope.tags = entry.TagTexts
    $scope.image = entry.Image
    $scope.map.marker.coords.latitude = entry.Latitude
    $scope.map.marker.coords.longitude = entry.Longitude
    $scope.map.center.latitude = entry.Latitude
    $scope.map.center.longitude = entry.Longitude
    $scope.link = entry.YoutubeLink
    $scope.Tid = entry.TemplateId
    $scope.Cid = entry.Category
    $http.get("../../Template/GetTemplates").success (data) ->
      $scope.templates = data
      $scope.template = {Id:$scope.Tid}
    $http.get("../../Home/GetCategories").success (data) ->
      $scope.categories = data
      $scope.category = {id: $scope.Cid }

  $scope.getPost = ->
    $http(
      method: "GET"
      url: "/Home/GetEntry/"+$scope.id
    ).then (successCallback = (response) ->
      $scope.fillData(response.data)
     )

  window.onbeforeunload = (event) ->
    if $scope.isUploading
      message = "Sure you want to leave?"
      event = window.event  if typeof event is "undefined"
      event.returnValue = message  if event
      message
  $scope.isUploading = false
  $scope.tags = []
  $scope.loadTags = (query) ->
    $http.get "/Home/GetTagsForAutocomplete/" + query
  $scope.addPost = ->
    $scope.isUploading = true
    q =
      Id : $scope.id
      Category: $scope.category.id
      TemplateId: $scope.template.Id
      Title: $scope.title
      Description: $scope.description
      Content: $scope.my_markdown
      TagTexts: $scope.tags
      Image: $scope.image
      Latitude: $scope.map.marker.coords.latitude
      Longitude: $scope.map.marker.coords.longitude
      YoutubeLink: $scope.link
    $http(
      method: "POST"
      url: "/Home/EditEntry"
      data: q
    ).then (successCallback = (response) ->
        $scope.isUploading = false
        $window.location.href = '/View/Index/'+$scope.id;
      )
 ]


angular.module("myApp").controller "commentsController", [ "$scope", "$http", ($scope, $http) ->
  window.setInterval (->
    $scope.getComments $scope.id
  ), 10000
  $scope.setLike = (id) ->
    q = id: id
    $http(
      method: "POST"
      url: "/view/SetLike/"
      data: q
    ).then (successCallback = (response) ->
      $scope.getComments($scope.id)
    )
  $scope.getComments = (id) ->
    $scope.id = id
    q = id: id
    $http(
      method: "POST"
      url: "/view/GetComments/"
      data: q
    ).then (successCallback = (response) ->
      $scope.comments = response.data
    )

  $scope.removeComment = (id) ->
    q = 
      id : id
    $http(
      method: "POST"
      url: "/view/RemoveComment/"
      data: q
    ).then (successCallback = (response) ->
      $scope.getComments $scope.id
    )
  $scope.addComment = (id) ->
    comment = document.getElementById("comment").value
    q =
      id: id
      comment: comment

    $http(
      method: "POST"
      url: "/view/AddComment/"
      data: q
    ).then (successCallback = (response) ->
      $scope.getComments id
    )
 ]


angular.module("myApp").directive "myYoutube", ($sce) ->
  restrict: "EA"
  scope:
    code: "="

  replace: true
  template: "<div style=\"height:400px;\"><iframe style=\"overflow:hidden;height:100%;width:100%\" width=\"100%\" height=\"100%\" src=\"{{url}}\" frameborder=\"0\" allowfullscreen></iframe></div>"
  link: (scope) ->
    scope.$watch "code", (newVal) ->
      newVal = newVal.split("v=")[1]
      scope.url = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + newVal)  if newVal


angular.module("myApp").controller "viewController", [ "$scope", "$http", "uiGmapGoogleMapApi","$window", ($scope, $http, uiGmapGoogleMapApi,$window) ->
  uiGmapGoogleMapApi.then (maps) ->
    angular.extend $scope,
      map:
        center:
          latitude: 42.3349940452867
          longitude: -71.0353168884369

        zoom: 11
        marker : 
          id : 0
          coords:
            latitude: null
            longitude: null
  $scope.Id = null
  $scope.setId = (id) ->
    $scope.Id = id
    $scope.getRate(id)
  $scope.editEntry = ->
    $window.location.href='/Home/EditEntry/'+$scope.Id.toString()
  $scope.deleteEntry = ->
    q = 
      id : $scope.Id
    $http(
        method: "POST"
        url: "/AccountUser/DeleteEntry/"
        data: q
      ).then (successCallback = (response) ->
        $window.location.href = '/';
      )
  $scope.rating = null
  $scope.getRate = (id) ->
    q = 
      Id: id
    $http(
        method: "POST"
        url: "/Home/GetRate/"
        data: q
      ).then (successCallback = (response) ->
        $scope.rating = response.data
      )
    $http(
        method: "POST"
        url: "/Home/GetUserRate/"
        data: q
      ).then (successCallback = (response) ->
        if response.data =="" then $scope.isRateSet = false else
          $scope.isRateSet = true
          $scope.isPositive = response.data
      )
  $scope.vote = (ispositive) ->
    q = 
      Id : $scope.Id
      isPositive : ispositive
    $http(
      method: "POST"
      url: "/Home/AddRate/"
      data: q
    ).then (successCallback = (response) ->
      $scope.getRate($scope.Id)
    )
]

angular.module("myApp").controller "entriesController", [ "$scope", "$http", ($scope, $http) ->
  $scope.page = 0
  $scope.entries = []
  $scope.loadMore = (id) ->
    q =
      id: id
      page: $scope.page
    $http(
      method: "POST"
      url: "/accountUser/GetEntries/"
      data: q
    ).then (successCallback = (response) ->      
      for entry in response.data
        $scope.entries.push(entry)
      $scope.page +=1
    )
 ]

angular.module("myApp").controller "byTagController", [ "$scope", "$http","$location", ($scope, $http,$location) ->
  $scope.page = 0
  $scope.tag = $location.absUrl().split(/[/ ]+/).pop()
  $scope.entries = []
  $scope.loadMore = (tag) ->
    q =
      tagName: $scope.tag
      page: $scope.page
    $http(
      method: "POST"
      url: "/View/GetEntriesByTag/"
      data: q
    ).then (successCallback = (response) ->      
      for entry in response.data
        $scope.entries.push(entry)
      $scope.page +=1
    )
 ]
 
angular.module("myApp").controller "homeController", [ "$scope", "$http", ($scope, $http) ->
  $scope.getEntries = ->
    $http(
      method: "Get"
      url: "/Home/Recent/"
    ).then (successCallback = (response) ->
      $scope.date = response.data
    )
 ]
 
angular.module("myApp").controller "popularEntriesController", [ "$scope", "$http", ($scope, $http) ->
  $scope.page = 0
  $scope.entries = []
  $scope.loadMore = ->
    q =
      page: $scope.page
    $http(
      method: "POST"
      url: "/View/MostPopular/"
      data: q
    ).then (successCallback = (response) ->      
      for entry in response.data
        $scope.entries.push(entry)
      $scope.page +=1
    )
 ]
 
angular.module("myApp").controller "categoriesController" ,[ "$scope", "$http", ($scope, $http) ->
  $scope.categories = []
  $http(
    method: "GET"
    url: "/Home/GetCategories/"
  ).then (successCallback = (response) ->      
    $scope.categories = response.data
  )
]


angular.module("myApp").controller "byCategoryController", [ "$scope", "$http","$location", ($scope, $http,$location) ->
  $scope.page = 0
  $scope.category = $location.absUrl().split(/[/ ]+/).pop()
  $scope.entries = []
  $scope.loadMore = ->
    q =
      id: $scope.category
      page: $scope.page
    $http(
      method: "POST"
      url: "/View/ByCategory/"
      data: q
    ).then (successCallback = (response) ->      
      for entry in response.data
        $scope.entries.push(entry)
      $scope.page +=1
    )
 ]

﻿using System.Linq;
using AutoMapper;
using EducationApp.Models;

namespace EducationApp.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<ApplicationUser, UserDetailsViewModel>();
            Mapper.CreateMap<UserDetailsViewModel, ApplicationUser>();
            Mapper.CreateMap<EntryAddViewModel, Entry>().ForMember(x => x.Tags, opt => opt.Ignore());
            Mapper.CreateMap<Entry, EntryViewModel>()
                .ForMember("Username", opt => opt.MapFrom(src => src.User.UserName))
                .ForMember("Tags", opt => opt.MapFrom(src => src.Tags.ToList()));
            Mapper.CreateMap<Entry, EntryEditViewModel>();
            Mapper.CreateMap<EntryEditViewModel, Entry>();
            Mapper.CreateMap<Comment, CommentViewModel>()
                .ForMember("Time", o => o.MapFrom(src => src.Time.ToString()))
                .ForMember("UserImage", o => o.MapFrom(src => src.User.ImageUrl))
                .ForMember("UserName", o => o.MapFrom(src => src.User.UserName))
                .ForMember("LikesCount", o => o.MapFrom(src => src.Likes.Count));


            Mapper.CreateMap<Entry, EntryPreviewViewModel>()
                .ForMember("Rating", o => o.ResolveUsing(Converter))
                .ForMember("CommentsCount", opt => opt.MapFrom(src => src.Comments.Count))
                .ForMember("Username", opt => opt.MapFrom(src => src.User.UserName))
                .ForMember("Tags", opt => opt.MapFrom(src => src.Tags.Select(x => x.TagName)));
        }

        private static object Converter(Entry value)
        {
            var rating = 0;
            foreach (var userRate in value.UserRates)
            {
                rating += userRate.IsPositive ? 1 : -1;
            }

            return rating;
        }
    }
}
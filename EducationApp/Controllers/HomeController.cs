﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EducationApp.Cloud;
using EducationApp.Filters;
using EducationApp.Lucene;
using EducationApp.Migrations;
using EducationApp.Models;
using EducationApp.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Controllers
{
    [Unban]
    [Culture]
    public class HomeController : Controller
    {
        private UnitOfWork _unitOfWork;
        private ApplicationUserManager _userManager;

        public HomeController()
        {
            _userManager = System.Web.HttpContext.Current.GetOwinContext()
                .GetUserManager<ApplicationUserManager>();
            _unitOfWork = new UnitOfWork();
        }

        public ActionResult Index()
        {
            var user = _userManager.FindById(User.Identity.GetUserId());

            string lang = "ru";
            if (user != null)
                if (user.Language != null)
                    lang = user.Language.ToString();
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang; 
            else
            {
                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
            }
            Response.Cookies.Add(cookie);
            return View();
        }

        public ActionResult GetTagsForCloud()
        {
            var lst = _unitOfWork.Tags.Get().Select(tag => new TagUsing {TagName = tag.TagName, Uses = tag.Entries.Count}).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetTagsForAutocomplete(string id)
        {
            var lst =
               (from tag in _unitOfWork.Tags.Get()
                where tag.TagName.StartsWith(id)
                select new TagText {text = tag.TagName}).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetEntry(int id)
        {
            var entry = _unitOfWork.Entries.GetById(id);
            var e = Mapper.Map<Entry, EntryEditViewModel>(entry);
            e.TagTexts = entry.Tags.Select(tag => new TagText { text = tag.TagName }).ToList();
            e.Image = "https://res.cloudinary.com/iedapp/image/upload/" + entry.ImageId;
            return Json(e, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult AddEntry()
        {
            return View();
        }


        [Authorize]
        public ActionResult EditEntry(int id)
        {
            var entry = _unitOfWork.Entries.GetById(id);
            if (!User.IsInRole("admin") && User.Identity.GetUserId() != entry.User.Id)
            {
                return View("EditError");
            }
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult EditEntry(EntryEditViewModel model)
        {
            
            var entry = _unitOfWork.Entries.GetById(model.Id);

            Mapper.Map(model, entry);
            var template = _unitOfWork.Templates.GetById(model.TemplateId);
            entry.Template = template;
            var tags = model.TagTexts;
            entry.Tags.Clear();
            foreach (var sq in tags)
            {
                var tag = _unitOfWork.Tags.Get(s => s.TagName.Equals(sq.text)).FirstOrDefault();
                if (tag == null)
                {
                    entry.Tags.Add(new Tag { TagName = sq.text });
                }
                else
                {
                    entry.Tags.Add(tag);
                }
            }
            if (entry.ImageId != model.Image)
            {
                var imageUploader = new ImageUploader();
                entry.ImageId = imageUploader.UploadBase64Image(model.Image);
            }
            _unitOfWork.Entries.Update(entry);
            _unitOfWork.Save();
            LuceneEntryModel.AddUpdateLuceneIndex(entry);
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddEntry(EntryAddViewModel model)
        {
            var entry = Mapper.Map<EntryAddViewModel, Entry>(model);
            var tags = model.Tags;
            var template = _unitOfWork.Templates.GetById(model.TemplateId);
            foreach (var tag in tags)
            {
                var currentTag = _unitOfWork.Tags.Get(s => s.TagName.Equals(tag.text)).FirstOrDefault();
                if (currentTag == null)
                {
                    entry.Tags.Add(new Tag {TagName = tag.text});
                }
                else
                {
                    entry.Tags.Add(currentTag);
                }
            }
            if (model.Image != null)
            {
                var imageUploader = new ImageUploader();
                entry.ImageId = imageUploader.UploadBase64Image(model.Image);
            }
            else
            {
                entry.ImageId = "noimage";
            }
            
            entry.Template = template;
            entry.CreateTime = DateTime.Now;

            var user = _userManager.FindById(User.Identity.GetUserId());
            user.Entries.Add(entry);

            var medal = _unitOfWork.Medals.GetById(5);
            if (!user.Medals.Contains(medal))
            {
                if (user.Entries.Count >= 10)
                {
                    user.Medals.Add(medal);
                }
            }
            _userManager.Update(user);
            LuceneEntryModel.AddUpdateLuceneIndex(entry);

            return Json(entry.Id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRate(int Id)
        {
            var edb = _unitOfWork.Entries.GetById(Id);
            int rating = edb.UserRates.Sum(userRate => userRate.IsPositive ? 1 : -1);
            return Json(rating,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUserRate(int Id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var entry = _unitOfWork.Entries.GetById(Id);
                var user = _userManager.FindById(User.Identity.GetUserId());
                var userRate = entry.UserRates.FirstOrDefault(x => x.UserId == user.Id);
                return Json(userRate?.IsPositive, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddRate(int Id, bool isPositive)
        {
            var entry = _unitOfWork.Entries.GetById(Id);
            var user = _userManager.FindById(User.Identity.GetUserId());

            foreach (var userRate in entry.UserRates)
            {
                if (userRate.UserId == user.Id)
                {
                    if (userRate.IsPositive != isPositive)
                    {
                        userRate.IsPositive = !userRate.IsPositive;
                        _unitOfWork.Entries.Update(entry);
                        _unitOfWork.Save();
                        return null;
                    }
                    else
                    {
                        entry.UserRates.Remove(userRate);
                        _unitOfWork.UserRates.Delete(userRate.Id);
                        _unitOfWork.Save();
                        _unitOfWork.Entries.Update(entry);
                        _unitOfWork.Save();
                        return null;
                    }
                }
            }
            entry.UserRates.Add(new UserRate { Entry = entry, IsPositive = isPositive, User = user });
            _unitOfWork.Entries.Update(entry);
            _unitOfWork.Save();
            return null;
        }

        public ActionResult Recent()
        {
            IEnumerable<Entry> sortedEntries = _unitOfWork.Entries.Get().ToList().OrderByDescending(x => x.CreateTime).Take(5);
            var lst =  Mapper.Map<IEnumerable<Entry>, IEnumerable<EntryPreviewViewModel>>(sortedEntries);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCategories()
        {

            var enumVals = new List<object>();

            foreach (var item in Enum.GetValues(typeof(Categories)))
            {

                enumVals.Add(new
                {
                    id = (int)item,
                    name = item.ToString()
                });
            }
            return Json(enumVals, JsonRequestBehavior.AllowGet);
        }

        public class TagUsing
        {
            public string TagName { get; set; }
            public int Uses { get; set; }
        }

        public class TagText
        {
            public string text { get; set; }
        }
    }
}
﻿using EducationApp.Lucene;
using EducationApp.Models;
using System;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Version = Lucene.Net.Util.Version;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Store;
using System.IO;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using System.Web.Mvc;
using EducationApp.Filters;

namespace EducationApp.Controllers
{
    [Unban]
    [Culture]
    public class SearchController : Controller
    {

        public ActionResult Index(string id)
        {
            List<Entry> findEntries = LuceneEntryModel.Search(id, null).ToList();
            return View(findEntries);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EducationApp.Filters;
using EducationApp.Lucene;
using EducationApp.Models;
using EducationApp.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Controllers
{
    [Unban]
    [Culture]
    public class AccountUserController : Controller
    {

        private UnitOfWork _unitOfWork;
        private ApplicationUserManager _userManager;

        public AccountUserController()
        {
            _userManager = System.Web.HttpContext.Current.GetOwinContext()
    .GetUserManager<ApplicationUserManager>();
            _unitOfWork = new UnitOfWork();
        }

        public ActionResult User(string id)
        {
            var user = _userManager.FindByName(id);
            return View(user);
        }

        public ActionResult GetEntries(string id, int page)
        {
            int pagesize = 5;
            var user = _userManager.FindById(id);
            var entryPage = user.Entries.Reverse().Skip(page * pagesize).Take(pagesize);
            var entryPreviewPage = Mapper.Map<IEnumerable<Entry>, IEnumerable<EntryPreviewViewModel>>(entryPage);
            return Json(entryPreviewPage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteEntry(int id)
        {
            var entry = _unitOfWork.Entries.GetById(id);
            LuceneEntryModel.ClearLuceneIndexRecord(entry.Id);
            _unitOfWork.Entries.Delete(entry);
            _unitOfWork.Save();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }
}
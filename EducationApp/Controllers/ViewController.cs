﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EducationApp.Filters;
using EducationApp.Models;
using EducationApp.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Controllers
{
    [Unban]
    [Culture]
    public class ViewController : Controller
    {
        private UnitOfWork _unitOfWork;

        public ViewController()
        {
            _unitOfWork = new UnitOfWork();
        }

        public ActionResult GetEntriesByTag(string tagName, int page)
        {
            var tagText = HttpUtility.UrlDecode(tagName);
            int pagesize = 5;
            var tag = _unitOfWork.Tags.Get(x => x.TagName.Equals(tagText)).Single();
            var entries = _unitOfWork.Entries.Get(x => x.Tags.Select(q=>q.TagName).Contains(tagText));
            List<EntryPreviewViewModel> entriesPreviewModel = new List<EntryPreviewViewModel>();
            foreach (var e in entries.Skip(page * pagesize).Take(pagesize))
            {
                var entryPreviewModel = Mapper.Map<Entry, EntryPreviewViewModel>(e);
                entryPreviewModel.CreateTime = entryPreviewModel.CreateTime.ToString();
                entriesPreviewModel.Add(entryPreviewModel);
            }
            return Json(entriesPreviewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ByTag(string id)
        {
            return View();
        }

        public ActionResult Index(int id)
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var edb = db.Entries.FirstOrDefault(s => s.Id.Equals(id));
            var e = Mapper.Map<Entry, EntryViewModel>(edb);
            return View(e);
        }

        public ActionResult RemoveComment(int id)
        {
            if (User.IsInRole("admin"))
            {
                var comment = _unitOfWork.Comments.GetById(id);
                _unitOfWork.Comments.Delete(comment);
                _unitOfWork.Save();
            }
            return null;
        }

        public ActionResult AddComment(int id, string comment)
        {
            Comment c = new Comment();
            c.EntryId = id;
            c.Text = comment;
            c.Time = DateTime.Now;
            c.User = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            db.Comments.Add(c);
            db.SaveChanges();
            var medal = db.Medals.Single(x => x.Id.Equals(4));
            if (!c.User.Medals.Contains(medal))
            {
                if (c.User.Comments.Count >= 10)
                {
                    c.User.Medals.Add(medal);
                }
            }
            db.SaveChanges();
            return Json(comment, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetLike(int id)
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var comment = db.Comments.FirstOrDefault(s => s.Id.Equals(id));
            var user =
     System.Web.HttpContext.Current.GetOwinContext()
         .GetUserManager<ApplicationUserManager>()
         .FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            if (comment != null)
            {
                var like = comment.Likes.FirstOrDefault(s => s.UserId.Equals(user.Id));
                if (like == null)
                {
                    comment.Likes.Add(new Like() {User = user});
                    db.Entry(comment).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    comment.Likes.Remove(like);
                    db.Entry(like).State = EntityState.Deleted;
                    db.Entry(comment).State = EntityState.Modified;
                    db.SaveChanges();
                }
            
            }
            return null;
        }

        public ActionResult GetComments(int id)
        {
           var comments = _unitOfWork.Comments.Get(x => x.EntryId == id);
           var commentViewModels = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModel>>(comments);
           return Json(commentViewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MostPopular(int page)
        {
            int pagesize = 5;
            var entryPage = _unitOfWork.Entries.Get().OrderByDescending(d => d.UserRates.Sum(userRate => userRate.IsPositive ? 1 : -1))
                .Skip(page*pagesize)
                .Take(pagesize);
            var entryPreviewPage =  Mapper.Map<IEnumerable<Entry>, IEnumerable<EntryPreviewViewModel>>(entryPage);
            return Json(entryPreviewPage, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ByCategory(Categories id, int page)
        {
            int pagesize = 5;
            var entryPage = _unitOfWork.Entries.Get(x => x.Category.ToString().Equals(id.ToString()))
                .Skip(page * pagesize)
                .Take(pagesize);
            var entryPreviewPage = Mapper.Map<IEnumerable<Entry>, IEnumerable<EntryPreviewViewModel>>(entryPage);
            return Json(entryPreviewPage, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ByCategory(Categories id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult MostPopular()
        {
            return View();
        }
    }
}
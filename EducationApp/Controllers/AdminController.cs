﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using EducationApp.Filters;
using EducationApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Unban]
    [Culture]
    public class AdminController : Controller
    {


        public ActionResult ShowUsers()
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var users = db.Users.ToList();
            return View(users);
        }

        public ActionResult Ban(string id)
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var user = db.Users.Where(x => x.UserName == id).Single();
            var medal = db.Medals.Single(x => x.Id.Equals(2));
            if (!user.Medals.Contains(medal))
            {
                user.Medals.Add(medal);

            }
            user.LockoutEndDateUtc = DateTime.Now.AddMonths(1);
            db.SaveChanges();
            return RedirectToAction("ShowUsers");
        }

        public ActionResult UnBan(string id)
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var user = db.Users.Where(x => x.UserName == id).Single();
            user.LockoutEndDateUtc = null;
            db.SaveChanges();
            return RedirectToAction("ShowUsers");
        }

        public ActionResult EditUser(string id)
        {
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var user = db.Users.Where(x => x.UserName == id).Single();
            var model = Mapper.Map<ApplicationUser, UserDetailsViewModel>(user);
            model.UserId = user.Id;
            return View(model);
        }

        public async Task<ActionResult> ResetPassword(string id)
        {
            var userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var newPassword = Guid.NewGuid().ToString().Substring(0, 15);

            var user = db.Users.Where(x => x.UserName == id).Single();

            if (user == null || await userManager.IsEmailConfirmedAsync(user.Id))
            {
                return View("ForgotPasswordConfirmation");
            }
            var code = await userManager.GeneratePasswordResetTokenAsync(user.Id);

            await userManager.SendEmailAsync(user.Id, "Сброс пароля",
                "Ваш пароль был сброшен. Текущий пароль: " + newPassword);
            var hashedNewPassword = userManager.PasswordHasher.HashPassword(newPassword);
            //UserStore<ApplicationUser> store = new UserStore<ApplicationUser>();
            //store.SetPasswordHashAsync(user, hashedNewPassword);
            //store.UpdateAsync(user);
            user.PasswordHash = hashedNewPassword;
            userManager.Update(user);
            return RedirectToAction("ShowUsers");
        }
    }
}
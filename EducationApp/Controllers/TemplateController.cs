﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EducationApp.Filters;
using EducationApp.Models;
using EducationApp.Repositories;
using Microsoft.AspNet.Identity.Owin;

namespace EducationApp.Controllers
{
    [Unban]
    [Culture]
    public class TemplateController : Controller
    {
        private UnitOfWork _unitOfWork;

        public TemplateController()
        {
            _unitOfWork = new UnitOfWork();
        }
        public ActionResult GetEditTemplate(int id)
        {
            var template = _unitOfWork.Templates.Get(x => x.Id.Equals(id)).FirstOrDefault();
            return PartialView(template.TemplateEdit);
        }

        public ActionResult GetTemplates()
        {
            var templates = _unitOfWork.Templates.Get().ToList();
            return Json(templates, JsonRequestBehavior.AllowGet);
        }

    }
}